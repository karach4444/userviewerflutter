import 'package:UsersViewer/Modeles/User.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class RequestsManager {
  Future<List<User>> fetchUsers(int numberOfUsers) async {
    final url = "https://randomuser.me/api/?results=$numberOfUsers";
    final response = await http.get(url);
    Iterable l = jsonDecode(response.body)['results'];
    return List<User>.from(l.map((userJson) => User.fromJson(userJson)));
  }
}