class User {
  String gender;
  Name name;
  Location location;
  String email;
  String phone;
  String cell;
  Picture picture;

  User(
      {this.gender,
        this.name,
        this.location,
        this.email,
        this.phone,
        this.cell,
        this.picture
      });

  User.fromJson(Map<String, dynamic> json) {
    gender = json['gender'];
    name = json['name'] != null ? new Name.fromJson(json['name']) : null;
    location = json['location'] != null
        ? new Location.fromJson(json['location'])
        : null;
    email = json['email'];
    phone = json['phone'];
    cell = json['cell'];
    picture = json['picture'] != null ? new Picture.fromJson(json['picture']) : null;
  }
}

class Name {
  String title;
  String first;
  String last;

  Name({this.title, this.first, this.last});

  Name.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    first = json['first'];
    last = json['last'];
  }
}

class Location {
  Street street;
  String city;

  Location(
      {this.street,
        this.city,
      });

  Location.fromJson(Map<String, dynamic> json) {
    street =
    json['street'] != null ? new Street.fromJson(json['street']) : null;
    city = json['city'];
  }
}

class Street {
  int number;
  String name;

  Street({this.number, this.name});

  Street.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    name = json['name'];
  }
}

class Picture {
  String large;
  String medium;
  String thumbnail;

  Picture({this.large, this.medium, this.thumbnail});

  Picture.fromJson(Map<String, dynamic> json) {
    large = json['large'];
    medium = json['medium'];
    thumbnail = json['thumbnail'];
  }
}