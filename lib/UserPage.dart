import 'package:UsersViewer/Modeles/User.dart';
import 'package:flutter/material.dart';

class UserPage extends StatelessWidget {
  final User user;
  final _fontSize = 20.0;

  const UserPage({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Center(
          child: Column(
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage(user.picture.large),
                radius: 128,
              ),
              Text(
                user.name.first + " " + user.name.last,
                style: TextStyle(fontSize: _fontSize, fontWeight: FontWeight.bold),

              ),
              Text(
                user.location.city,
                style: TextStyle(fontSize: _fontSize),
              ),
              Text(
                user.location.street.name + " " + user.location.street.number.toString(),
                style: TextStyle(fontSize: _fontSize),
              ),
              SizedBox(
                child: FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  child: Text("Send email to " + user.email),
                  onPressed: () => {},
                ),
                width: double.infinity,
              ),
              SizedBox(
                child: FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  child: Text("Call " + user.phone),
                  onPressed: () => {},
                ),
                width: double.infinity,
              ),
              SizedBox(
                child: FlatButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  child: Text("Call " + user.cell),
                  onPressed: () => {},
                ),
                width: double.infinity,
              ),
              SizedBox(
                child: FlatButton(
                    child: Text("Other details"),
                    onPressed: () => {},
                ),
                width: double.infinity,
              ),
            ],
          ),
        )
      ),
    );
  }
}

