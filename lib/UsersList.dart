import 'package:UsersViewer/Managers/RequestsApiManager.dart';
import 'package:UsersViewer/Modeles/User.dart';
import 'package:UsersViewer/UserPage.dart';
import 'package:flutter/material.dart';

class UsersList extends StatefulWidget {
  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  RequestsManager manager = RequestsManager();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Users List"),
        ),
        body: Container(
          child: FutureBuilder<List<User>>(
              future: manager.fetchUsers(100),
              builder: (BuildContext context, AsyncSnapshot<List<User>> snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                    padding: EdgeInsets.all(16),
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      return createUsersListTile(snapshot.data[index]);
                    },
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }
          ),
        )
    );
  }
  Widget createUsersListTile(User user) {
    return ListTile(
      contentPadding: EdgeInsets.all(5),
      leading: Image.network(user.picture.thumbnail),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(user.name.first + " " + user.name.last),
          Text(user.location.street.name + " " + user.location.street.number.toString()),
          Text(user.phone)
        ],
      ),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => UserPage(user: user)));
      },
    );
  }
}